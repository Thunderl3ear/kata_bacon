#include <iostream>
#include <chrono>
#include <thread>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
using json = nlohmann::json;
#include <filesystem>
#include <JetsonGPIO.h>

int main(int, char**) 
{
    GPIO::setmode(GPIO::BOARD); 
    GPIO::setup(19, GPIO::IN); 

    std::cout<<"Press the button to receive bacon"<<std::endl;

    int timer = 1000;

    while(GPIO::input(19) == GPIO::HIGH && timer>0)
    {
        timer--;
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    GPIO::cleanup();

    if(timer>0)
    {
        std::cout<<"Fetching Bacon"<<std::endl;
        std::string folder_name = "downloads";
        std::string mkdir = "mkdir -p "+ folder_name;
        system(mkdir.c_str());
        std::filesystem::path p1 {folder_name.c_str()};

        int count = 0;
        for (auto& p : std::filesystem::directory_iterator(p1))
        {
            ++count;
        }
        
        std::string fname = std::to_string(count)+"_bacon.jpg";

        RestClient::Response r = RestClient::get("https://imsea.herokuapp.com/api/1?q=bacon");

        auto json_body = json::parse(r.body);
        std::string url = json_body["results"][2*count+1];
        std::string wget = "wget -O "+folder_name+"/"+fname+" "+ url;
        std::string open = "eog "+ folder_name+"/"+fname;

        system(wget.c_str());

        // std::this_thread::sleep_for(std::chrono::milliseconds(500));

        system(open.c_str());
    }
    else
    {
        std::cout<<"No bacon requested"<<std::endl;
    }
}